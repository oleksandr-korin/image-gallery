class ImageItemController {
  constructor($scope) {
    this.$scope = $scope;
    $scope.imageStyle = this.getImageStyle();
  }

  getImageStyle() {
  	return {
  		background: `url(${this.$scope.source.url})`
  	}
  }

}

ImageItemController.$inject = ['$scope'];

export default ImageItemController;