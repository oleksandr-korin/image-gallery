import template from './image.html';
import controller from './image.controller';
import './image.styl';

let imageComponent = () => {
	return {
		restrict: 'E',
		scope: {
			source: '=source'
		},
		template,
		controller
	}
};

export default imageComponent;
