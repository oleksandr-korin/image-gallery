import imageController from './image.controller';

describe('image test', () => {
  let controller, $scope;

  beforeEach(() => {
    $scope = {
      source: {
        url: 'some-image-url' 
      }
    };

    controller = new imageController($scope);
  });

  it('should create style object for image',() => {
    expect(controller.getImageStyle())
          .to.have.property('background', 'url(' + $scope.source.url + ')');
  });
});

