import angular from 'angular';
import imageComponent from './image.component';

export default angular.module('imageItem', [])
	.directive('imageItem', imageComponent).name;