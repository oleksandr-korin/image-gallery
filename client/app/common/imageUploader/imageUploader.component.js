import template from './imageUploader.html';
import controller from './imageUploader.controller';

let imageUploaderComponent = {
  restrict: 'E',
  bindings: {},
  template,
  controller
};

export default imageUploaderComponent;
