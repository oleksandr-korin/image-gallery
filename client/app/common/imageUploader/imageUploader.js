import angular from 'angular';
import ngFileUpload from 'ng-file-upload';
import imageUploaderComponent from './imageUploader.component';
import ImageSourceService from './imageSource.service';


export default angular.module('imageUploader', [
  ngFileUpload
])
.service('ImageSourceService', ImageSourceService)
.component('imageUploader', imageUploaderComponent).name;