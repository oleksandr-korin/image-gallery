class ImageModel {
  constructor() {
    this.id;
    this.url;
    this.description;
  }
}

export default ImageModel;