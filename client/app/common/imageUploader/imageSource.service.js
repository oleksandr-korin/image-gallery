import ImageModel from './image.model';

class ImageSourceService {
  constructor(Upload, $rootScope, $http) {
    this.$http = $http;
    this.Upload = Upload;
    this.preloadedImages = [];

    this.imgUrls = [
      'http://www.outdoor-photos.com/_photo/2047286.jpg',
      'http://s1.favim.com/orig/24/beach-landscape-mountains-nature-ocean-Favim.com-218308.jpg',
      'http://sf.co.ua/14/05/wallpaper-897384.jpg',
      'http://www.stoplusjednicka.cz/sites/default/files/clanky/2014/09/more.jpg',
      'http://mostnature.com/wp-content/uploads/2016/08/sky-ocean-clouds-mountains-fantastic-forests-sea-sunset-earth-skies-wallpaper-1366x768.jpg',
      'http://s4.favim.com/orig/50/friends-fun-mountains-ocean-photography-Favim.com-459478.jpg',
      'http://takashi1016.com/wp-content/uploads/2013/11/pine-island-glacier.jpg',
      'http://i.dailymail.co.uk/i/pix/2016/08/09/11/3707B01B00000578-3731167-image-m-64_1470739787418.jpg',
      'http://www.pd4pic.com/images/mountains-rocks-sea-ocean-water-sunset-landscape.jpg',
      'http://www.outdoor-photos.com/_photo/2047286.jpg',
      'http://s1.favim.com/orig/24/beach-landscape-mountains-nature-ocean-Favim.com-218308.jpg',
      'http://sf.co.ua/14/05/wallpaper-897384.jpg'
    ];

    this.imageRemoteUrl = 'remote/images/';
    this.imagesUploadUrl = 'remote/uploadImages/';
  }

  getRemoteImages() {
    // Uncomment once server interaction is added
    // return this.$http({
    //   method: 'GET',
    //   url: this.imageRemoteUrl,
    // });

    let imageEntities = this.imgUrls.map((imageItem) => this.createImageEntity(imageItem));

    return new Promise((resolve) => resolve(imageEntities));
  }

  createImageEntity(imageItem) {
    let image = new ImageModel();

      image.id = imageItem.id || '';
      image.url = imageItem.url || imageItem.$ngfBlobUrl || imageItem;
      image.description = imageItem.description || 'description';

      return image;
  }

  getPreloadedImages() {
    let imageEntities = [];

    this.preloadedImages.forEach((image) => {
      imageEntities.push(this.createImageEntity(image));
    });

    return imageEntities;
  }

  addPreloadedImages(images) {
    images.forEach((image) => {
      this.preloadedImages.push(image);
    });
  }

  deleteRemoteImage(image) {

    // Uncomment once server interaction is added
    // return this.$http({
    //   method: 'DELETE',
    //   url: this.imageRemoteUrl,
    //   data: image
    // });

    // to be removed after above is true
    return new Promise((resolve) => {
      setTimeout(() => resolve('deleted!!!'), 2000);
    });
  }

  deletePreloadedImage(image) {
    this.preloadedImages = this.preloadedImages.filter((img) => img.$ngfBlobUrl !== image.url);
  }

  editImageDescription(image) {

    // Uncomment once server interaction is added
    // return this.$http({
    //   method: 'PUT',
    //   url: this.imageRemoteUrl,
    //   data: image
    // });

    // to be removed after above is true
    return new Promise((resolve) => {
      setTimeout(() => resolve('edited!!!'), 2000);
    });
  }

  uploadImages(images) {
    return this.Upload.upload({
        url: this.imagesUploadUrl,
        data: {
            images: images
        }
    });
  }

}

ImageSourceService.$inject = ['Upload', '$rootScope', '$http'];

export default ImageSourceService;