class ImageUploaderController {
  constructor($rootScope, $scope, ImageSourceService) {
    this.$rootScope = $rootScope;

    this.ImageSourceService = ImageSourceService;
  }

  onImageSelect() {
    setTimeout(() => {
  		this.ImageSourceService.addPreloadedImages(this.images);
  		this.$rootScope.$broadcast('ImagesPreloaded');
    }, 2000);
  }

  uploadImages() {
    this.ImageSourceService.uploadImages(this.images);
  }
}

ImageUploaderController.$inject = ['$rootScope', '$scope', 'ImageSourceService'];

export default ImageUploaderController;