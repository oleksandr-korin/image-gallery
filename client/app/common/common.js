import angular from 'angular';
import Navbar from './navbar/navbar';
import Hero from './hero/hero';
import ImageUploader from './imageUploader/imageUploader';
import ImageList from './imageList/imageList';
import Tooltip from './tooltip/tooltip';

export default angular.module('app.common', [
  Navbar,
  Hero,
  ImageList,
  ImageUploader,
  Tooltip
]).name;