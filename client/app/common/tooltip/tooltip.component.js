import template from './tooltip.html';
import controller from './tooltip.controller';
import './tooltip.styl';

let tooltipComponent = () => {
	return {
		restrict: 'E',
		scope: {
			description: '=description'
		},
		template,
		controller,
		link: (scope, el) => {
			let tirgger = angular.element(el).find('span'),
				tooltip = angular.element(el).find('p');

			// tirgger.on('click', () => {
			tirgger.on('mouseenter mouseleave', () => {
				tooltip.toggleClass('shown');
			});
		}
	}
};

export default tooltipComponent;
