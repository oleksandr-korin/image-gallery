import angular from 'angular';
import tooltipComponent from './tooltip.component';

export default angular.module('tooltip', [])
	.directive('tooltip', tooltipComponent).name;