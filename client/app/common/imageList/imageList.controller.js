class ImageListController {
	constructor($rootScope, $scope, ImageSourceService) {
		this.ImageSourceService = ImageSourceService;

		ImageSourceService.getRemoteImages()
			.then((images) => {
				this.imageList = images;
			});

		$rootScope.$on('ImagesPreloaded', () => {
			this.preloadedImageList = ImageSourceService.getPreloadedImages();
			$scope.$apply();
		});
	}

	deletePreloadedImage(image) {
		this.ImageSourceService.deletePreloadedImage(image);
		this.preloadedImageList = this.ImageSourceService.getPreloadedImages();
	}

	deleteImage(image) {
		this.ImageSourceService.deleteRemoteImage(image)
			.then((response) => {
				return this.ImageSourceService.getRemoteImages();
			}).then((images) => {
				console.log('Deleted!!!');
				this.imageList = images;
			});
	}

	edit(image) {
		image.editMode = true;
	}

	saveEdit(image, isPreloaded) {
		if (!isPreloaded) {
			this.ImageSourceService.editImageDescription(image)
				.then(() => {
					console.log('Edited!!!');
				});
		}
		delete image.editMode;
	}
}

ImageListController.$inject = ['$rootScope', '$scope', 'ImageSourceService'];

export default ImageListController;