import angular from 'angular';
import uiRouter from 'angular-ui-router';
import ImageComponent from '../image/image'
import imageListComponent from './imageList.component';

export default angular.module('imageList', [
		ImageComponent
	])
	.component('imageList', imageListComponent)
	.config(($stateProvider, $urlRouterProvider) => {
		"ngInject";

		$urlRouterProvider.otherwise('/');

		$stateProvider.state('imageList', {
			url: '/',
			component: 'imageList'
		});
	})
	.name;