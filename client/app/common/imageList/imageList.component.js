import template from './imageList.html';
import controller from './imageList.controller';
// import './imageList.styl';

let imageListComponent = {
  restrict: 'E',
  bindings: {},
  template,
  controller
};

export default imageListComponent;
