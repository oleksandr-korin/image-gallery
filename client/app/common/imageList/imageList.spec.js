import ImageSourceService from '../imageUploader/ImageSource.service';
import imageListController from './imageList.controller';

describe('image list test', () => {
  let controller, $scope, $rootScope, ImgSourceService;
    ImgSourceService = new ImageSourceService();

  beforeEach(() => {
    $scope = {
      $apply: () => {}
    };

    $rootScope = {
      $on: () => {}
    };

    controller = new imageListController($rootScope, $scope, ImgSourceService);
  });

  it('should have a list of images', (done) => {
    done();

    expect(controller.imageList)
          .to.be.an('array').to.be.empty();
  });

});